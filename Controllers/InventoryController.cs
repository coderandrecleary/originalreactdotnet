using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ReactDotNet.Services;

namespace ReactDotNet.Controllers
{

    [Route("v1/")]
    [ApiController]
    public class InventoryController : ControllerBase
    {
        private readonly IInventoryServices _services;

        public InventoryController(IInventoryServices services)
        {
            _services = services;
        }

        [HttpPost]
        [Route("AddInventoryItems")]
        public ActionResult<InventoryItems> AddInventoryItems(InventoryItems items)
        {
            var inventoryItems = _services.AddInventoryItems(items);

            if (inventoryItems == null)
            {
                return NotFound();
            }
            return (ActionResult<InventoryItems>)inventoryItems;
        }


        [HttpGet]
        [Route("GetInventoryItems")]
        public ActionResult<Dictionary<string, InventoryItems>> GetInventoryItems()
        {

            var inventoryItems = _services.GetInventoryItems();

            if (inventoryItems.Count == 0)
            {
                NotFound();
            }
            return inventoryItems;
        }
    }
}