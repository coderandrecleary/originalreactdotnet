using System;
using System.Linq;
using System.Collections.Generic;


namespace ReactDotNet.Services
{

    public class InventoryServices : IInventoryServices
    {
        private readonly InventoryContext context;


        public InventoryServices(InventoryContext context)
        {
            this.context = context;
        }

        public InventoryItems AddInventoryItems(InventoryItems items)
        {
            this.context.InventoryItems.Add(items);
            try
            {
                this.context.SaveChanges();

            }
            catch (Exception e)
            {
                Console.WriteLine("" + e.Message);
            }

            return items;
        }

        public Dictionary<string, InventoryItems> GetInventoryItems()
        {
            return this.context.InventoryItems.ToDictionary(x => x.ItemName);
        }

    }
}