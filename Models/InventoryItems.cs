using Microsoft.EntityFrameworkCore;


namespace ReactDotNet
{

    public class InventoryContext : DbContext
    {
        public DbSet<InventoryItems> InventoryItems { get; set; }

        //Constructor
        public InventoryContext(DbContextOptions<InventoryContext> options) : base(options) { }
        public InventoryContext() : base() { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }

    }


    public class InventoryItems
    {
        public int Id { get; set; }
        public string ItemName { get; set; }
        public double Price { get; set; }
    }
}