import React, { Component } from "react";
import { Route } from "react-router";
import { Layout } from "./components/Layout";
import { Home } from "./components/Home";
import { FetchData } from "./components/FetchData";
import { Counter } from "./components/Counter";

import "./App.css";

export default class App extends Component {
  static displayName = App.name;

  render() {
    return (
      <Layout>
        <Route exact path="/" component={Home} />
        <Route path="/counter" component={Counter} />
        <Route path="/fetch-data" component={FetchData} />
      </Layout>
    );
  }
}

// import React, { Component } from "react";
// import "./App.css";

// const Items = ({ items }) => {
//   return (
//     <div>
//       <center>
//         <h1>Inventory Service</h1>
//       </center>
//       {items.map(item => (
//         <div class="card">
//           <div class="card-body">
//             <h5 class="card-title">{items.Id}</h5>
//             <h6 class="card-subtitle mb-2 text-muted">{items.ItemName}</h6>
//             <p class="card-text">{items.Price}</p>
//           </div>
//         </div>
//       ))}
//     </div>
//   );
// };

// class App extends React.Component {
//   constructor() {
//     super();
//     this.state = {
//       items: []
//     };

//     // this.onSubmit = this.onSubmit.bind(this);
//   }

//   // https://localhost:5001/v1/GetInventoryItems
//   // https://localhost:5001/v1/AddInventoryItems
//   async componentDidMount() {
//     const response = await fetch("https://localhost:5001/v1/GetInventoryItems");
//     const json = await response.json();
//     this.setState({ items: json });
//   }

//   render() {
//     return console.log(Items);
//   }
// }

// export default App;
