import "./index.css";
import * as React from "react";
import axios from "axios";
import NumberFormat from "react-number-format";
import Button from "@material-ui/core/Button";

class InventoryService extends React.Component {
  //Constructor initiate component properties
  constructor() {
    super();
    this.state = {
      loading: true,
      Id: "",
      ItemName: "",
      Price: "",
      data: []
    };
    this.onChange = this.onChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.changeHandler = this.changeHandler.bind(this);
    this.componentDidCatch = this.componentDidCatch.bind(this);
    this.getInventory = this.getInventory.bind(this);
    this.resetTextField = this.resetTextField.bind(this);
  }
  onChange(e) {
    const re = /^[0-9\b]+$/;
    // if value is not blank, then test the regex
    if (e.target.value === "" || re.test(e.target.value)) {
      this.setState({ Id: e.target.value });
    }
  }
  changeHandler = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidCatch(error, errorInfo) {
    console.log(error, errorInfo);
  }
  async getInventory() {
    const url = "https://localhost:5001/v1/GetInventoryItems";
    const response = await fetch(url);
    const data = await response.json();
    const obj = Object.entries(data).map(([key, value]) => ({ key, value }));
    this.setState({ data: obj, loading: false });
    console.log(obj);
  }

  resetTextField = e => {
    e.preventDefault();
    this.setState({ Id: "", ItemName: "", Price: "" });
  };
  handleSubmit = event => {
    alert("Form has been submitted");
    console.log("this.state", this.state);
    event.preventDefault();
    var inventoryItem = {};
    inventoryItem.Id = parseInt(this.state.Id);
    inventoryItem.ItemName = this.state.ItemName;
    inventoryItem.Price = parseFloat(this.state.Price.slice(1));
    console.log(inventoryItem.Price);
    var options = {
      method: "POST",
      url: "https://localhost:5001/v1/AddInventoryItems",
      data: inventoryItem,
      headers: { "content-type": "application/json; charset=utf-8" }
    };
    axios(options)
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      });
  };
  NumberFormat = require("react-number-format");
  render() {
    const { Id, ItemName, Price, data } = this.state;
    return (
      <div className="App" className="body">
        <form
          ref="form"
          onSubmit={this.handleSubmit}
          method="post"
          name="form1"
          id="form1"
        >
          <div>
            <header>
              <h1>
                <strong>Inventory Service Application</strong>
              </h1>
            </header>
          </div>
          <div>
            <label className="App-header">
              <strong>Identification Number:</strong>
              <input
                value={Id}
                onChange={this.onChange}
                name="Id"
                placeholder="ID"
              />
            </label>
          </div>
          <div>
            <label className="App-header">
              <strong>Item Name:</strong>
              <input
                type="text"
                name="ItemName"
                value={ItemName}
                onChange={this.changeHandler}
                placeholder="ITEM NAME"
              />
            </label>
          </div>
          <div>
            <label className="App-header">
              <strong>Price:</strong>
              <NumberFormat
                thousandSeparator={true}
                prefix={"$"}
                name="Price"
                value={Price}
                onChange={this.changeHandler}
                placeholder="PRICE"
              />
            </label>
          </div>
          <div>
            <Button variant="outlined" type="submit">
              Submit
            </Button>

            <div class="divider" />
            <Button
              onClick={this.resetTextField}
              variant="outlined"
              type="reset"
            >
              RESET
            </Button>
          </div>
          <div>
            {this.state.loading || !this.state.data ? (
              <div>loading...</div>
            ) : (
              <div className="centered">
                <table key="inventoryList">
                  <thead>
                    <tr>
                      <th style={{ textAlign: "center" }}>ID</th>
                      <th style={{ textAlign: "center" }}>Item Name</th>
                      <th style={{ textAlign: "center" }}>Price</th>
                    </tr>
                  </thead>
                  <tbody>
                    {data.map((x, key) => {
                      console.log(x.value.id);
                      return (
                        <tr key={x.value.id}>
                          <td>{x.value.id}</td>
                          <td>{x.value.itemName}</td>
                          <td>{x.value.price}</td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            )}
          </div>
          <br />
          <div>
            <Button
              variant="outlined"
              color="primary"
              onClick={this.getInventory}
            >
              Get Inventory Items
            </Button>
          </div>
        </form>
      </div>
    );
  }
}
export default InventoryService;
