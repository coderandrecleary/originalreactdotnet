import React from "react";
import ReactDOM from "react-dom";
import { MemoryRouter } from "react-router-dom";
import App from "./App";
import InventoryService from "./InventoryService";

it("renders without crashing", async () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <MemoryRouter>
      {/* <App /> */}
      <InventoryService />
    </MemoryRouter>,
    div
  );
  await new Promise(resolve => setTimeout(resolve, 1000));
});
